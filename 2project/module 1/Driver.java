public class Driver {
    private String name;
    private String carModel;
    private String carPlateNumber;

    public Driver(String name, String carModel, String carPlateNumber) {
        this.name = name;
        this.carModel = carModel;
        this.carPlateNumber = carPlateNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getCarModel() {
        return carModel;
    }

    public String getCarPlateNumber() {
        return carPlateNumber;
    }

    public void setCarPlateNumber(String carPlateNumber) {
        this.carPlateNumber = carPlateNumber;
    }

    @Override
    public String toString() {
        return "Driver{" +
                "name='" + name + '\'' +
                ", carModel='" + carModel + '\'' +
                ", carPlateNumber='" + carPlateNumber + '\'' +
                '}';
    }
}
