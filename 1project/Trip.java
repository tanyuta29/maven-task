public class Trip {
   private String client;
   private String driver;
   private double distance;
   private double price;
   public Trip(String client, String driver, double distance, double price){
       this.client=client;
       this.driver=driver;
       this.distance=distance;
       this.price=price;
   }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getClient() {
        return client;
    }

    public String getDriver() {
        return driver;
    }

    public void setClient(String client) {
        this.client = client;
    }

    @Override
    public String toString() {
        return "Trip{" +
                "client='" + client + '\'' +
                ", driver='" + driver + '\'' +
                ", distance=" + distance +
                ", price=" + price +
                '}';
    }
}

